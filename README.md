## Taranta one command setup

Intended to facilitate local development. Subject to change.

### Requirements

* Make
* git
* Docker
* docker-compose

### Usage

Execute

    $ make run

and go to http://localhost:22484/testdb in your web browser.

### Local frontend development

Enter the frontend project directory:

    $ cd taranta

Change the proxy settings in `src/setupProxy.js` to:

    const proxies = {
      "/testdb/socket": {
        target: "http://localhost:22484",
        secure: false,
        changeOrigin: true,
        ws: true
      },
      "/testdb/db": {
        target: "http://localhost:22484",
        secure: false,
        changeOrigin: true
      },
      "/auth": {
        target: "http://localhost:22484",
        secure: false,
        changeOrigin: true
      },
      "/dashboards": {
        target: "http://localhost:22484",
        secure: false,
        changeOrigin: true
      }
    };

Install dependencies:

    $ npm install

Start the development server:

    $ npm start
